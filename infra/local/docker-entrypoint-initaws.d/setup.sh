#!/usr/bin/env bash

awslocal secretsmanager create-secret --name api-cookie --secret-string "{\"key\": \"dontactuallyusethisforproductionyoumotlyfoolitsfordev\"}"
awslocal secretsmanager create-secret --name api-jwt --secret-string "{\"key\": \"dontactuallyusethisforproductionyoumotlyfoolitsfordev\"}"
awslocal secretsmanager create-secret --name api-database --secret-string "{\"url\": \"postgres://dbowner:password123@database/api\"}"

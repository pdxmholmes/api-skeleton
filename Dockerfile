# Build container
FROM node:14 AS build

WORKDIR /var/build
COPY config ./config
COPY src ./src
COPY .eslintrc.js .prettierrc.js jest.config.js .
COPY package.json tsconfig.json yarn.lock .

RUN yarn install --pure-lockfile && yarn ci

# Execute container
FROM node:14-slim AS execute

ENV NODE_ENV=production

WORKDIR /var/service
COPY --from=build /var/build/dist ./dist
COPY --from=build /var/build/config ./config
COPY --from=build /var/build/package.json .
COPY --from=build /var/build/yarn.lock .
RUN yarn install --pure-lockfile --prod

COPY docker-entrypoint.sh /
ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD ["start-service"]

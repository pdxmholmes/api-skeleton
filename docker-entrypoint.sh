#!/bin/bash

set -e
if [ "$1" = "start-service" ]; then
  exec node /var/service/dist/index.js
fi

exec "$@"

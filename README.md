## API Skeleton

A small skeleton that contains the very basics for a TypeScript Fastify HTTP/S service. Contains the most basic
development environment for the service with linting, prettier and TypeScript builds. Contains core of a CI/CD
environment with a Dockerfile using multi-stage builds.

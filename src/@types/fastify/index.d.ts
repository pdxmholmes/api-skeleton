import Ajv from 'ajv';
import { Redis } from 'ioredis';
import { IDatabase } from 'pg-promise';
import { Settings } from '../../plugins/settings';
import { Secrets } from '../../plugins/secrets';

declare module 'fastify' {
  interface FastifyInstance {
    cache: Redis;
    // eslint-disable-next-line @typescript-eslint/ban-types
    database: IDatabase<{}>;
    settings: Settings;
    secrets: Secrets;
    validator: Ajv;
  }
}

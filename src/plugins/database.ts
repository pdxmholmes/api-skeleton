import { FastifyPluginAsync } from 'fastify';
import fp from 'fastify-plugin';
import pg from 'pg-promise';

const plugin: FastifyPluginAsync = async app => {
  const pgp = pg();
  const database = pgp(app.secrets.database.url);
  app.decorate('database', database);
};

export default fp(plugin, {
  name: 'database',
  fastify: '>=3.x',
  dependencies: ['secrets'],
  decorators: {
    fastify: ['secrets']
  }
});

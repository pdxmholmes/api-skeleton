import { EnvironmentCredentials, SecretsManager } from 'aws-sdk';
import { FastifyPluginAsync } from 'fastify';
import fp from 'fastify-plugin';
import BPromise from 'bluebird';
import { Type, Static } from '@sinclair/typebox';

export interface SecretsDictionary {
  [idx: string]: any;
}

const Secrets = Type.Object({
  cookie: Type.Object({
    key: Type.String()
  }),
  jwt: Type.Object({
    key: Type.String()
  }),
  database: Type.Object({
    url: Type.String()
  })
});
export type Secrets = Static<typeof Secrets>;

const plugin: FastifyPluginAsync = async app => {
  const loadSecrets = async (): Promise<Secrets> => {
    const sm = new SecretsManager({
      ...(app.settings.aws || {}),
      credentials: !process.env.AWS_SDK_LOAD_CONFIG ? new EnvironmentCredentials('AWS') : undefined
    });

    const secrets: SecretsDictionary = {};
    await BPromise.map(app.settings.secrets, async secretDef => {
      const secretValue = await sm
        .getSecretValue({
          SecretId: secretDef.secretId
        })
        .promise();

      if (!secretValue || !secretValue.SecretString) {
        throw new Error(`could not load secret: ${secretDef.name}`);
      }

      secrets[secretDef.name] = JSON.parse(secretValue.SecretString);
    });

    return secrets as Secrets;
  };

  const secrets = await loadSecrets();
  if (!app.validator.validate(Secrets, secrets)) {
    throw new Error(`secrets invalid, failed validation\n${JSON.stringify(app.validator.errors, null, 2)}`);
  }

  app.decorate('secrets', secrets);
};

export default fp(plugin, {
  name: 'secrets',
  fastify: '>=3.x',
  dependencies: ['validation', 'settings'],
  decorators: {
    fastify: ['validator', 'settings']
  }
});

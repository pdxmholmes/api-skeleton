import Ajv from 'ajv';
import addFormats from 'ajv-formats';
import { FastifyPluginAsync } from 'fastify';
import fp from 'fastify-plugin';

const plugin: FastifyPluginAsync = async app => {
  const ajv = addFormats(new Ajv(), [
    'date-time',
    'time',
    'date',
    'email',
    'hostname',
    'ipv4',
    'ipv6',
    'uri',
    'uri-reference',
    'uuid',
    'uri-template',
    'json-pointer',
    'relative-json-pointer',
    'regex'
  ])
    .addKeyword('kind')
    .addKeyword('modifier');

  app.decorate('validator', ajv);
};

export default fp(plugin, {
  name: 'validation',
  fastify: '>=3.x'
});

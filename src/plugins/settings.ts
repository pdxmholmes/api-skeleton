import config, { IConfig } from 'config';
import { FastifyPluginAsync } from 'fastify';
import fp from 'fastify-plugin';
import { omit } from 'lodash';
import { Type, Static } from '@sinclair/typebox';

const SecretDefinition = Type.Object({
  name: Type.String({ minLength: 1 }),
  secretId: Type.String({ minLength: 1 })
});
export type SecretDefinition = Static<typeof SecretDefinition>;

const Settings = Type.Object({
  port: Type.Optional(Type.Number()),
  bindAddress: Type.Optional(Type.String()),
  baseUri: Type.String({ minLength: 1 }),
  authRedirect: Type.String({ minLength: 1 }),
  cookieDomain: Type.String({ minLength: 1 }),
  cacheUrl: Type.String({ minLength: 1 }),
  secrets: Type.Array(SecretDefinition, { minItems: 1 }),
  aws: Type.Optional(
    Type.Object({
      region: Type.Optional(Type.String()),
      endpoint: Type.Optional(Type.String())
    })
  )
});
export type Settings = IConfig & Static<typeof Settings>;

const plugin: FastifyPluginAsync = async app => {
  const settings = config as Settings;

  if (!app.validator.validate(Settings, omit(settings, ['util', 'get', 'has']))) {
    throw new Error(`settings invalid, failed validation\n${JSON.stringify(app.validator.errors, null, 2)}`);
  }

  app.decorate('settings', settings);
};

export default fp(plugin, {
  name: 'settings',
  fastify: '>=3.x',
  dependencies: ['validation'],
  decorators: {
    fastify: ['validator']
  }
});

import Redis from 'ioredis';
import { FastifyPluginAsync } from 'fastify';
import fp from 'fastify-plugin';

const plugin: FastifyPluginAsync = async app => {
  const cache = new Redis(app.settings.cacheUrl);
  app.decorate('cache', cache);
};

export default fp(plugin, {
  name: 'cache',
  fastify: '>=3.x',
  dependencies: ['settings'],
  decorators: {
    fastify: ['settings']
  }
});

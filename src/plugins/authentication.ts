import { FastifyPluginAsync, FastifyRequest } from 'fastify';
import fp from 'fastify-plugin';
import Cookie from 'fastify-cookie';
import Jwt from 'fastify-jwt';

const plugin: FastifyPluginAsync = async app => {
  await app.register(Cookie, {
    secret: app.secrets.cookie.key
  });
  await app.register(Jwt, {
    secret: app.secrets.jwt.key
  });

  app.decorate('authenticate', async (req: FastifyRequest) => {
    try {
      req.jwtVerify();
    } catch {
      throw app.httpErrors.unauthorized();
    }
  });
};

export default fp(plugin, {
  name: 'authentication',
  fastify: '>=3.x',
  dependencies: ['secrets', 'cache', 'database'],
  decorators: {
    fastify: ['secrets', 'cache', 'database']
  }
});

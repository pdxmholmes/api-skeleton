import Fastify, { FastifyInstance } from 'fastify';
import GracefulShutdown from 'fastify-graceful-shutdown';
import Helmet from 'fastify-helmet';
import Sensible from 'fastify-sensible';

import Authentication from './plugins/authentication';
import Settings from './plugins/settings';
import Secrets from './plugins/secrets';
import Validation from './plugins/validation';
import Database from './plugins/database';
import Cache from './plugins/cache';

import StatusRoutes from './routes/status';

export default async function (logger = true): Promise<FastifyInstance> {
  const app = Fastify({ logger });

  app.log.info('registering plugins...');
  await app.register(Sensible);
  await app.register(Helmet);
  await app.register(GracefulShutdown);

  await app.register(Validation);
  await app.register(Settings);
  await app.register(Secrets);
  await app.register(Cache);
  await app.register(Database);
  await app.register(Authentication);

  app.log.info('registering routes...');
  await app.register(StatusRoutes);

  return app;
}

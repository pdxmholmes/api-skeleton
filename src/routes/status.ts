import { FastifyPluginAsync } from 'fastify';

const plugin: FastifyPluginAsync = async app => {
  app.get('/status', async () => ({
    status: 'running',
    phrase: 'AOkayChief'
  }));
};

export default plugin;

import makeServerApp from './app';

async function main(): Promise<void> {
  const server = await makeServerApp();
  const port = server.settings.port || 3000;
  const bindAddress = server.settings.bindAddress || '0.0.0.0';

  await server.listen(port, bindAddress);
}

main().catch(err => {
  console.error(err);
  process.exit(1);
});
